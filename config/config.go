// Copyright 2020 The Gitea Authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package config

import (
	_ "embed"
	"os"
	"regexp"

	"gopkg.in/yaml.v2"
)

//go:embed changelog.example.yml
var DefaultConfig []byte

// Group is a grouping of PRs
type Group struct {
	Name    string   `yaml:"name"`
	Labels  []string `yaml:"labels"`
	Default bool     `yaml:"default"`
}

// NameLabel is a Group mapping for a Label to a Name
type NameLabel struct {
	Name  string
	Label string
}

// NameLabels returns a slice of NameLabel
func (g Group) NameLabels() []NameLabel {
	nl := make([]NameLabel, 0)
	for _, l := range g.Labels {
		nl = append(nl, NameLabel{
			Name:  g.Name,
			Label: l,
		})
	}
	return nl
}

// Config is the changelog settings
type Config struct {
	Repo       string         `yaml:"repo"`
	Service    string         `yaml:"service"`
	BaseURL    string         `yaml:"base-url"`
	Groups     []Group        `yaml:"groups"`
	SkipLabels string         `yaml:"skip-labels"`
	SkipRegex  *regexp.Regexp `yaml:"-"`
}

// NameLabels returns a slice of NameLabel for each Group, keeping them in order (priority)
func (c Config) NameLabels() []NameLabel {
	nl := make([]NameLabel, 0)
	for _, g := range c.Groups {
		nl = append(nl, g.NameLabels()...)
	}
	return nl
}

// New Load a config from a path, defaulting to changelog.example.yml
func New(configPath string) (*Config, error) {
	var err error
	configContent := DefaultConfig
	if len(configPath) != 0 {
		configContent, err = os.ReadFile(configPath)
		if err != nil {
			return nil, err
		}
	}

	var cfg *Config
	if err = yaml.Unmarshal(configContent, &cfg); err != nil {
		return nil, err
	}

	if len(cfg.SkipLabels) > 0 {
		if cfg.SkipRegex, err = regexp.Compile(cfg.SkipLabels); err != nil {
			return nil, err
		}
	}

	if cfg.Service == "" {
		cfg.Service = "github"
	}

	return cfg, nil
}
