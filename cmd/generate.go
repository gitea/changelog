// Copyright 2020 The Gitea Authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package cmd

import (
	"fmt"
	"regexp"
	"strings"

	"code.gitea.io/changelog/config"
	"code.gitea.io/changelog/service"

	"github.com/urfave/cli/v3"
)

var Generate = &cli.Command{
	Name:   "generate",
	Usage:  "Generates a changelog for a special milestone",
	Action: runGenerate,
}

func runGenerate(ctx *cli.Context) error {
	cfg, err := config.New(configPathFlag)
	if err != nil {
		return err
	}

	s, err := service.New(cfg.Service, cfg.Repo, cfg.BaseURL, milestoneFlag, tagFlag, tokenFlag, issuesFlag)
	if err != nil {
		return err
	}

	title, prs, err := s.Generate(ctx.Context)
	if err != nil {
		return err
	}

	var defaultGroup string
	for _, g := range cfg.Groups {
		if g.Default {
			defaultGroup = g.Name
		}
	}
	if defaultGroup == "" {
		fmt.Println("<!-- WARNING - no default group found -->")
	}

	entries := processPRs(prs, cfg.NameLabels(), defaultGroup, cfg.SkipRegex)

	fmt.Println(title)
	fmt.Println()
	for _, g := range cfg.Groups {
		if len(entries[g.Name]) == 0 {
			continue
		}

		if detailsFlag {
			fmt.Println("<details><summary>" + g.Name + "</summary>")
			fmt.Println()
			for _, entry := range entries[g.Name] {
				fmt.Printf("* %s (#%d)\n", entry.Title, entry.Index)
			}
			fmt.Println("</details>")
		} else {
			fmt.Println("* " + g.Name)
			for _, entry := range entries[g.Name] {
				fmt.Printf("  * %s (#%d)\n", entry.Title, entry.Index)
			}
		}
	}

	return nil
}

func processPRs(prs []service.Entry, order []config.NameLabel, defaultGroup string, skip *regexp.Regexp) map[string][]service.Entry {
	entries := make(map[string][]service.Entry)
PRLoop: // labels in Go, let's get old school
	for _, pr := range prs {
		if pr.Index < afterFlag {
			continue
		}

		for _, lb := range pr.Labels {
			if skip != nil && skip.MatchString(lb.Name) {
				continue PRLoop
			}
		}
		section := processSection(pr, order, defaultGroup)
		if section == "" {
			continue
		}
		entries[section] = append(entries[section], pr)
	}
	return entries
}

func processSection(pr service.Entry, order []config.NameLabel, defaultGroup string) string {
	for _, o := range order {
		for _, lb := range pr.Labels {
			if strings.EqualFold(o.Label, lb.Name) {
				return o.Name
			}
		}
	}
	return defaultGroup
}
